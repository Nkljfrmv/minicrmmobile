package com.example.nikolaj.minicrmmobile.db.repository;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.table.tabs.ClientsFragment;
import com.example.nikolaj.minicrmmobile.table.tabs.ContactPersonsFragment;
import com.example.nikolaj.minicrmmobile.table.tabs.MakersFragment;
import com.example.nikolaj.minicrmmobile.table.tabs.OrdersFragment;

public class DetailActivity extends AppCompatActivity {

    public static String SWITCH = "switch";
    public static String ID = "id";
    private MakersFragment makersFragment;
    private OrdersFragment ordersFragment;
    private ClientsFragment clientsFragment;
    private ContactPersonsFragment personsFragment;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        FragmentTransaction ft = null;
        switch (getIntent().getExtras().getString(SWITCH)) {
            case "order_to_persons":
                personsFragment = new ContactPersonsFragment();
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.container,personsFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
                toolbar.setTitle(R.string.contact_persons_tab);
                break;

            case "client_to_orders":
                ordersFragment = new OrdersFragment();
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.container, ordersFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
                toolbar.setTitle(R.string.orders_tab);
                break;

            case "manager_to_clients":
                clientsFragment = new ClientsFragment();
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.container,clientsFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
                toolbar.setTitle(R.string.clients_tab);
                break;
            case "maker_to_orders":
                ordersFragment = new OrdersFragment();
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.container, ordersFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
                toolbar.setTitle(R.string.orders_tab);
                break;
            case "client_to_persons":
                personsFragment = new ContactPersonsFragment();
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.container,personsFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
                toolbar.setTitle(R.string.contact_persons_tab);
                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }
}
