package com.example.nikolaj.minicrmmobile.db.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.db.DBHelper;
import com.example.nikolaj.minicrmmobile.db.pojo.ContactPerson;

import java.util.ArrayList;
import java.util.List;

public class ContactPersonRepository {

    private Context context;

    private final String TABLE_NAME = "contact_persons";

    private String selectList = "SELECT * FROM" + TABLE_NAME +";";
    private String selectId = "SELECT * FROM " + TABLE_NAME + " WHERE _id=";
    private String selectIdClient = "SELECT * FROM " + TABLE_NAME + " WHERE client_id=";
    private String selectLastId = "SELECT _id FROM " + TABLE_NAME + " ORDER BY _id DESC LIMIT 1";

    public ContactPersonRepository(Context context) {
        this.context = context;
    }

    public boolean addContactPerson(ContactPerson person) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();
            ContentValues personValues = new ContentValues();

            personValues.put("name", person.getName());
            personValues.put("surname", person.getSurname());
            personValues.put("patronymic", person.getPatronymic());
            personValues.put("phone_number", person.getPhoneNumber());
            personValues.put("e_mail", person.getEMail());
            personValues.put("other_means", person.getOtherMeans());
            personValues.put("position", person.getPosition());
            personValues.put("notes", person.getNotes());
            personValues.put("client_id", person.getClientId());

            db.insert(TABLE_NAME, null, personValues);
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }


    public boolean updateContactPerson(ContactPerson person) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();
            ContentValues personValues = new ContentValues();

            personValues.put("name", person.getName());
            personValues.put("surname", person.getSurname());
            personValues.put("patronymic", person.getPatronymic());
            personValues.put("phone_number", person.getPhoneNumber());
            personValues.put("e_mail", person.getEMail());
            personValues.put("other_means", person.getOtherMeans());
            personValues.put("position", person.getPosition());
            personValues.put("notes", person.getNotes());

            db.update(TABLE_NAME,
                    personValues,
                    "_id = ?",
                    new String[] {Integer.toString(person.getId())});
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }

    public boolean deleteContactPerson(int id) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();

            db.delete(TABLE_NAME,
                    "_id = ?",
                    new String[] {Integer.toString(id)});
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }

    public ContactPerson findById(int id) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectId+Integer.toString(id)+";", null);
            if (cursor.moveToFirst())
                return getNextPerson(cursor);
        }catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }

    public int getLastId() {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectLastId, null);
            if (cursor.moveToFirst())
                return cursor.getInt(0);
        } catch (SQLException e) {
            Toast.makeText(context, R.string.trouble_db, Toast.LENGTH_SHORT).show();
        } finally {
            db.close();
            helper.close();
        }
        return -1;
    }

    public List<ContactPerson> getListPersons() {
        return getListByQuery(selectList);
    }

    public List<ContactPerson> findByClientId(int clientId) {
        return getListByQuery(selectIdClient+Integer.toString(clientId)+";");
    }

    public List<ContactPerson> getListByQuery(String sql) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            List<ContactPerson> persons = new ArrayList<>();
            if (cursor.moveToFirst()) {
                for (int i = 0; i < cursor.getCount(); i ++) {
                    persons.add(getNextPerson(cursor));
                    cursor.moveToNext();
                }
            }
            return persons;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }

    private ContactPerson getNextPerson(Cursor cursor) {
        int id = cursor.getInt(0);
        String name = cursor.getString(1);
        String surname = cursor.getString(2);
        String patronymic = cursor.getString(3);
        String phoneNumber  = cursor.getString(4);
        String eMail = cursor.getString(5);
        String otherMeans = cursor.getString(6);
        String position = cursor.getString(7);
        String notes = cursor.getString(8);
        int clientId = cursor.getInt(9);

        return new ContactPerson(id,clientId, name, surname, patronymic, phoneNumber, eMail,
                otherMeans, position, notes);
    }

}
