package com.example.nikolaj.minicrmmobile.cards;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.nikolaj.minicrmmobile.MainActivity;
import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.databinding.ActivityOrderCardBinding;
import com.example.nikolaj.minicrmmobile.db.pojo.Order;
import com.example.nikolaj.minicrmmobile.db.repository.ClientRepository;
import com.example.nikolaj.minicrmmobile.db.repository.DetailActivity;
import com.example.nikolaj.minicrmmobile.db.repository.MakerRepository;
import com.example.nikolaj.minicrmmobile.db.repository.OrderRepository;

import java.util.Calendar;
import java.util.List;

public class OrderCardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private boolean isNew;
    public static final String ID = "position";

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigation;
    private FrameLayout header;
    private ActivityOrderCardBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_card);
        binding.setContext(this);
        toolbar = binding.toolbar;
        drawer = binding.drawer;

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.open_nav_view, R.string.close_nav_view);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        List<Integer> makerIds = new MakerRepository(this).getIds();
        ArrayAdapter<?> adapterMakerIds = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                makerIds);
        binding.editIdMaker.setAdapter(adapterMakerIds);

        List<Integer> clientIds = new ClientRepository(this).getIds();
        ArrayAdapter<?> adapterClientIds = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                clientIds);
        binding.editIdClient.setAdapter(adapterClientIds);

        createDatePicker(binding.dateStart);
        createDatePicker(binding.dateFinish);

        navigation = findViewById(R.id.navigation_view);
        navigation.setNavigationItemSelectedListener(this);

        header = navigation.getHeaderView(0).findViewById(R.id.header);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                Intent intent = new Intent(OrderCardActivity.this, HeadCardActivity.class);
                startActivity(intent);
            }
        });

        try {
            getIntent().getExtras().getInt(ID);
            isNew = false;
            OrderRepository orderRepository = new OrderRepository(this);
            Order order = orderRepository.findById(getIntent().getExtras().getInt(ID));
            binding.setOrder(order);
            binding.editIdClient.setSelection(clientIds.indexOf(order.getClientId()));
            binding.editIdMaker.setSelection(makerIds.indexOf(order.getMakerId()));
        } catch (Exception e) {
            isNew = true;
            binding.setOrder(new Order());
        }

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.delete:
                        if (binding.getOrder().getId() == 0){
                            Snackbar.make(toolbar.getRootView(), "Нет записи", Snackbar.LENGTH_SHORT).show();

                        } else {
                            new OrderRepository(toolbar.getContext()).deleteOrder(binding.getOrder().getId());
                            Snackbar.make(toolbar.getRootView(), "Запись удалена", Snackbar.LENGTH_SHORT).show();
                            binding.setOrder(new Order());
                            binding.editIdClient.setSelection(0);
                            binding.editIdMaker.setSelection(0);
                            isNew = true;
                        }
                }
                return false;
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        onBackPressed();
        Intent intent = null;
        if (isNew) Snackbar.make(toolbar.getRootView(), "Нет записи", Snackbar.LENGTH_SHORT).show();
        else switch (menuItem.getItemId()) {
            case R.id.to_client:
                intent = new Intent(this, ClientCardActivity.class);
                intent.putExtra(ClientCardActivity.ID, binding.getOrder().getClientId());
                startActivity(intent);
                return true;
            case R.id.to_contact_persons:
                intent = new Intent(this, DetailActivity.class);
                intent.putExtra(DetailActivity.ID, binding.getOrder().getClientId());
                intent.putExtra(DetailActivity.SWITCH, "order_to_persons");
                startActivity(intent);
                return true;

            case R.id.to_maker:
                intent = new Intent(this, MakerCardActivity.class);
                intent.putExtra(MakerCardActivity.ID, binding.getOrder().getMakerId());
                startActivity(intent);
                return true;
        }
        if (menuItem.getItemId() == R.id.to_home) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    public void onClickFloatingButton(View view) {
        if ((binding.editIdClient.getCount() == 0) || (binding.editIdMaker.getCount() == 0)) {
            Snackbar.make(view,"Проверьте наличие разработчика или клиента",Snackbar.LENGTH_SHORT).show();
        } else {
            OrderRepository orderRepository = new OrderRepository(this);
            Order order = binding.getOrder();
            order.setIdClient((int)binding.editIdClient.getSelectedItem());
            order.setIdMaker((int)binding.editIdMaker.getSelectedItem());
            if (isNew) {
                orderRepository.addOrder(order);
                binding.editId.setText(Integer.toString(orderRepository.getLastId()));
                Snackbar.make(view,"Запись добавлена",Snackbar.LENGTH_SHORT).show();
                isNew = false;
            } else {
                orderRepository.udpateOrder(order);
                Snackbar.make(view,"Запись обновлена",Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    private void createDatePicker(final TextView dateView) {
        final DatePickerDialog.OnDateSetListener listener;

        listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month++;
                String date = day + ":" + month + ":" + year;
                dateView.setText(date);
            }
        };

        dateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        OrderCardActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        listener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

}
