package com.example.nikolaj.minicrmmobile.db.pojo;

public class Client {

    private int id;
    private int managerId;
    private int status;
    private int type;
    private String log;
    private String notes;

    public Client(int id, int managerId, int status, int type, String log, String notes) {
        this.id = id;
        this.managerId = managerId;
        this.status = status;
        this.type = type;
        this.log = log;
        this.notes = notes;
    }

    public Client(int managerId, int status, int type, String log, String notes) {
        this.managerId = managerId;
        this.status = status;
        this.type = type;
        this.log = log;
        this.notes = notes;
    }

    public Client() {
        status = 0;
        type = 0;
        log = "";
        notes = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
