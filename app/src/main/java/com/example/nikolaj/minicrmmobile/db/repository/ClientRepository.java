package com.example.nikolaj.minicrmmobile.db.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.db.DBHelper;
import com.example.nikolaj.minicrmmobile.db.pojo.Client;

import java.util.ArrayList;
import java.util.List;

public class ClientRepository {

    private Context context;

    private final String TABLE_NAME = "clients";

    private String selectList = "SELECT * FROM " + TABLE_NAME +";";
    private String selectId = "SELECT * FROM " + TABLE_NAME +" WHERE _id =";
    private String selectIdManager= "SELECT * FROM " + TABLE_NAME +" WHERE manager_id =";
    private String selectStatus = "SELECT * FROM " + TABLE_NAME +" WHERE status = ";
    private String selectType = "SELECT * FROM " + TABLE_NAME +" WHERE type =";
    private String selectLastId = "SELECT _id FROM " + TABLE_NAME + " ORDER BY _id DESC LIMIT 1";

    public ClientRepository(Context context) {
        this.context = context;
    }

    public boolean AddClient(Client client) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();
            ContentValues clientValues = new ContentValues();

            clientValues.put("status", client.getStatus());
            clientValues.put("type", client.getType());
            clientValues.put("log", client.getLog());
            clientValues.put("notes", client.getNotes());
            clientValues.put("manager_id", Integer.toString(client.getManagerId()));

            db.insert(TABLE_NAME, null, clientValues);
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, R.string.trouble_db, Toast.LENGTH_SHORT).show();
        } finally {
            db.close();
            helper.close();
        }
        return false;
    }

    public boolean updateClient(Client client) {

        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();
            ContentValues clientValues = new ContentValues();

            clientValues.put("status", client.getStatus());
            clientValues.put("type", client.getType());
            clientValues.put("log", client.getLog());
            clientValues.put("notes", client.getNotes());
            clientValues.put("manager_id", Integer.toString(client.getManagerId()));

            db.update(TABLE_NAME,
                    clientValues,
                    "_id = ?",
                    new String[] {Integer.toString(client.getId())});
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, R.string.trouble_db, Toast.LENGTH_SHORT).show();
        } finally {
            db.close();
            helper.close();
        }
        return false;
    }

    public boolean deleteClient(int id) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();

            db.delete(TABLE_NAME,
                    "_id = ?",
                    new String[] {Integer.toString(id)});
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, R.string.trouble_db, Toast.LENGTH_SHORT).show();
        } finally {
            db.close();
            helper.close();
        }
        return false;
    }

    public Client findById(int id) {

        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectId +Integer.toString(id)+";", null);
            if (cursor.moveToFirst())
                return getNextClient(cursor);
        } catch (SQLException e) {
            Toast.makeText(context, R.string.trouble_db, Toast.LENGTH_SHORT).show();
        } finally {
            db.close();
            helper.close();
        }
        return null;

    }

    public List<Client> getListClients() {
        return getListByQuery(selectList);

    }

    public List<Client> findByIdManager(int managerId) {
        return getListByQuery(selectIdManager + "\"" + Integer.toString(managerId) + "\";");
    }

    public List<Client> findByStatus(int status) {
        return getListByQuery(selectStatus + "\"" + status + "\"");
    }

    public List<Client> findByType(int type) {
        return getListByQuery(selectType + "\"" + type + "\"");
    }

    public List<Integer> getIds() {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT _id FROM " +TABLE_NAME +";", null);
            List<Integer> ids = new ArrayList<>();
            if (cursor.moveToFirst());
            for (int i = 0; i < cursor.getCount(); i++){
                ids.add(cursor.getInt(0));
                cursor.moveToNext();
            }
            return ids;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }

    public int getLastId() {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectLastId, null);
            if (cursor.moveToFirst())
                return cursor.getInt(0);
        } catch (SQLException e) {
            Toast.makeText(context, R.string.trouble_db, Toast.LENGTH_SHORT).show();
        } finally {
            db.close();
            helper.close();
        }
        return -1;

    }

    private List<Client> getListByQuery(String sql) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            List<Client> clients = new ArrayList<>();
            if (cursor.moveToFirst()) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    clients.add(getNextClient(cursor));
                    cursor.moveToNext();
                }
            }
            return clients;
        } catch (SQLException e) {
            Toast.makeText(context, R.string.trouble_db, Toast.LENGTH_SHORT).show();

        }
        db.close();
        helper.close();
        return null;
    }


    private Client getNextClient(Cursor cursor) {
        int id = cursor.getInt(0);
        int status = cursor.getInt(1);
        int type = cursor.getInt(2);
        String log = cursor.getString(3);
        String notes = cursor.getString(4);
        int managerId = cursor.getInt(5);

        return new Client(id, managerId, status, type, log, notes);
    }
}
