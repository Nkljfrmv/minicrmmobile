package com.example.nikolaj.minicrmmobile.table.tabs;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.adapters.recyclerview.ContactPersonAdapter;
import com.example.nikolaj.minicrmmobile.cards.ContactPersonCardActivity;
import com.example.nikolaj.minicrmmobile.db.repository.DetailActivity;
import com.example.nikolaj.minicrmmobile.db.repository.ContactPersonRepository;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactPersonsFragment extends Fragment {

    RecyclerView recyclerView;

    public ContactPersonsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        FrameLayout frameLayout = (FrameLayout) inflater.inflate(R.layout.fragment_contact_person,
                container, false);
        recyclerView = frameLayout.findViewById(R.id.contact_persons_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        FloatingActionButton floatingButton = frameLayout.findViewById(R.id.floating_button);
        floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ContactPersonCardActivity.class);
                intent.putExtra(DetailActivity.ID, getActivity().getIntent().getExtras().getInt(DetailActivity.ID));
                startActivity(intent);
            }
        });
        return frameLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        filterByClientId(getActivity().getIntent().getExtras().getInt(DetailActivity.ID));
    }

    public void filterByClientId(int id) {
        ContactPersonRepository personRepository = new ContactPersonRepository(getContext());
        ContactPersonAdapter adapter = new ContactPersonAdapter(personRepository.findByClientId(id));
        recyclerView.setAdapter(adapter);
    }
}
