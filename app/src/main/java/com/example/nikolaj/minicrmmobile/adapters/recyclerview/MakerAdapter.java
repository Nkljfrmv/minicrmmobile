package com.example.nikolaj.minicrmmobile.adapters.recyclerview;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.adapters.CardViewBindingAdapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.cards.MakerCardActivity;
import com.example.nikolaj.minicrmmobile.databinding.TableItemMakerBinding;
import com.example.nikolaj.minicrmmobile.db.pojo.Maker;

import java.util.List;

public class MakerAdapter extends RecyclerView.Adapter<MakerAdapter.ViewHolder>{

    private List<Maker> makers;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TableItemMakerBinding binding;

        public ViewHolder(TableItemMakerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
        public void bind(Maker maker){
        binding.setMaker(maker);
        binding.executePendingBindings();
        }
    }

    public MakerAdapter(List<Maker> makers) {
        this.makers = makers;
    }

    @Override
    public int getItemCount() {
        return makers.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        TableItemMakerBinding binding = DataBindingUtil.inflate(inflater, R.layout.table_item_maker, parent, false);
        return new ViewHolder(binding);
    }

    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.bind(makers.get(position));
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MakerCardActivity.class);
                intent.putExtra(MakerCardActivity.ID, makers.get(position).getId());
                v.getContext().startActivity(intent);
            }
        });
    }
}
