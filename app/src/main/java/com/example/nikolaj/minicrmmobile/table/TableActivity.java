package com.example.nikolaj.minicrmmobile.table;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.cards.ClientCardActivity;
import com.example.nikolaj.minicrmmobile.cards.HeadCardActivity;
import com.example.nikolaj.minicrmmobile.cards.MakerCardActivity;
import com.example.nikolaj.minicrmmobile.cards.ManagerCardActivity;
import com.example.nikolaj.minicrmmobile.cards.OrderCardActivity;
import com.example.nikolaj.minicrmmobile.table.tabs.ClientsFragment;
import com.example.nikolaj.minicrmmobile.table.tabs.MakersFragment;
import com.example.nikolaj.minicrmmobile.table.tabs.ManagersFragment;
import com.example.nikolaj.minicrmmobile.table.tabs.OrdersFragment;

public class TableActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private SectionsPagerAdapter pagerAdapter;
    private ViewPager pager;
    private NavigationView navigation;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private FrameLayout header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        pagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        pager = findViewById(R.id.view_pager);
        pager.setAdapter(pagerAdapter);
        pager.setOffscreenPageLimit(4);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(pager);

        toolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.open_nav_view, R.string.close_nav_view);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        
        navigation = findViewById(R.id.navigation_view);
        navigation.setNavigationItemSelectedListener(this);
        header = navigation.getHeaderView(0).findViewById(R.id.header);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                Intent intent = new Intent(TableActivity.this, HeadCardActivity.class);
                startActivity(intent);

            }
        });
    }

    public void onClickFloatingButton(View view){
        int page = pager.getCurrentItem();
        Intent intent = new Intent();
        switch (page) {
            case 0:
                intent.setClass(this, OrderCardActivity.class);
                startActivity(intent);
                break;
            case 1:
                intent.setClass(this, ClientCardActivity.class);
                startActivity(intent);
                break;
            case 2:
                intent.setClass(this, ManagerCardActivity.class);
                startActivity(intent);
                break;
            case 3:
                intent.setClass(this, MakerCardActivity.class);
                startActivity(intent);
                break;
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.orders_all:
                pagerAdapter.getOrdersFragment().getAllOrders();
                pager.setCurrentItem(0);

                break;

            case R.id.order_id:
                pagerAdapter.getOrdersFragment().filterById();
                pager.setCurrentItem(0);

                break;

            case R.id.orders_status:
                pagerAdapter.getOrdersFragment().filterByStatus();
                pager.setCurrentItem(0);

                break;

            case R.id.orders_type:
                pagerAdapter.getOrdersFragment().filterByType();
                pager.setCurrentItem(0);

                break;

            case R.id.orders_maker_id:
                pagerAdapter.getOrdersFragment().filterByIdMaker();
                pager.setCurrentItem(0);

                break;

            case R.id.orders_client_id:
                pagerAdapter.getOrdersFragment().filterByIdClient();
                pager.setCurrentItem(0);
                break;

            case R.id.clients_all:
                pagerAdapter.getClientsFragment().getAllClients();
                pager.setCurrentItem(1);
                break;

            case R.id.client_id:
                pagerAdapter.getClientsFragment().filterById();
                pager.setCurrentItem(1);
                break;

            case R.id.clients_status:
                pagerAdapter.getClientsFragment().filterByStatus();
                pager.setCurrentItem(1);
                break;

            case R.id.clients_type:
                pagerAdapter.getClientsFragment().filterByType();
                pager.setCurrentItem(1);
                break;

            case R.id.clients_manager_id:
                pagerAdapter.getClientsFragment().filterByManagerId();
                pager.setCurrentItem(1);
                break;

            case R.id.managers_all:
                pagerAdapter.getManagersFragment().getAllManagers();
                pager.setCurrentItem(2);
                break;

            case R.id.manager_id:
                pagerAdapter.getManagersFragment().filterById();
                pager.setCurrentItem(2);
                break;

            case R.id.managers_name:
                pagerAdapter.getManagersFragment().filterByName();
                pager.setCurrentItem(2);
                break;

            case R.id.makers_all:
                pagerAdapter.getMakersFragment().getAllMaker();
                pager.setCurrentItem(3);
                break;

            case R.id.maker_id:
                pagerAdapter.getMakersFragment().filterbyId();
                pager.setCurrentItem(3);
                break;

            case R.id.makers_name:
                pagerAdapter.getMakersFragment().filterByName();
                pager.setCurrentItem(3);
                break;
        }
        onBackPressed();
        return true;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        private OrdersFragment ordersFragment;
        private ClientsFragment clientsFragment;
        private ManagersFragment managersFragment;
        private MakersFragment makersFragment;

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return ordersFragment = new OrdersFragment();
                case 1:
                    return clientsFragment = new ClientsFragment();
                case 2:
                    return managersFragment = new ManagersFragment();
                case 3:
                    return makersFragment = new MakersFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getText(R.string.orders_tab);
                case 1:
                    return getResources().getText(R.string.clients_tab);
                case 2:
                    return getResources().getText(R.string.managers_tab);
                case 3:
                    return getResources().getText(R.string.makers_tab);
            }
            return null;
        }

        public OrdersFragment getOrdersFragment() {
            return ordersFragment;
        }

        public ClientsFragment getClientsFragment() {
            return clientsFragment;
        }

        public ManagersFragment getManagersFragment() {
            return managersFragment;
        }

        public MakersFragment getMakersFragment() {
            return makersFragment;
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
