package com.example.nikolaj.minicrmmobile.table.tabs;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.adapters.recyclerview.ClientAdapter;
import com.example.nikolaj.minicrmmobile.cards.ClientCardActivity;
import com.example.nikolaj.minicrmmobile.db.repository.DetailActivity;
import com.example.nikolaj.minicrmmobile.db.repository.ClientRepository;
import com.example.nikolaj.minicrmmobile.db.repository.ManagerRepository;


public class ClientsFragment extends Fragment {

    private RecyclerView recyclerView;

    public ClientsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_client,
                container, false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        return recyclerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            switch (getActivity().getIntent().getExtras().getString(DetailActivity.SWITCH)) {
                case "manager_to_clients":
                    filterByManagerId(getActivity().getIntent().getExtras().getInt(DetailActivity.ID));
                    break;
            }
        } catch (Exception e) {
            getAllClients();
        }
    }

    public void getAllClients() {
        ClientRepository clientRepository = new ClientRepository(getContext());
        ClientAdapter clientAdapter = new ClientAdapter(clientRepository.getListClients());
        recyclerView.setAdapter(clientAdapter);
    }

    public void filterById() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_NUMBER);

        builder
                .setView(input)
                .setTitle("Введите ID")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getContext(), ClientCardActivity.class);
                        intent.putExtra(ClientCardActivity.ID, Integer.parseInt(input.getText().toString()));
                        getContext().startActivity(intent);
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    public void filterByStatus() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final Spinner status = new Spinner(getContext());
        ArrayAdapter<?> adapter = ArrayAdapter.createFromResource(
                getContext(),
                R.array.client_status,
                android.R.layout.simple_spinner_dropdown_item
        );
        status.setAdapter(adapter);

        builder
                .setView(status)
                .setTitle("Выберете статус")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ClientRepository clientRepository = new ClientRepository(getContext());
                        ClientAdapter adapter = new ClientAdapter(clientRepository.findByStatus(status.getSelectedItemPosition()));
                        recyclerView.setAdapter(adapter);
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    public void filterByType() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final Spinner type = new Spinner(getContext());
        ArrayAdapter<?> adapter = ArrayAdapter.createFromResource(
                getContext(),
                R.array.client_type,
                android.R.layout.simple_spinner_dropdown_item
        );
        type.setAdapter(adapter);

        builder
                .setView(type)
                .setTitle("Выберете Тип")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ClientRepository clientRepository = new ClientRepository(getContext());
                        ClientAdapter adapter = new ClientAdapter(clientRepository.findByType(type.getSelectedItemPosition()));
                        recyclerView.setAdapter(adapter);
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    public void filterByManagerId() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final Spinner managerId = new Spinner(getContext());
        ArrayAdapter<?> adapter = new ArrayAdapter<>(
                getContext(),
                android.R.layout.simple_spinner_dropdown_item,
                new ManagerRepository(getContext()).getIds()
        );
        managerId.setAdapter(adapter);

        builder
                .setView(managerId)
                .setTitle("Выберете ID менеджера")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ClientRepository clientRepository = new ClientRepository(getContext());
                        ClientAdapter adapter = new ClientAdapter(clientRepository.findByIdManager((int)managerId.getSelectedItem()));
                        recyclerView.setAdapter(adapter);
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    public void filterByManagerId(int id) {
        ClientRepository clientRepository = new ClientRepository(getContext());
        ClientAdapter adapter = new ClientAdapter(clientRepository.findByIdManager(id));
        recyclerView.setAdapter(adapter);
    }
}

