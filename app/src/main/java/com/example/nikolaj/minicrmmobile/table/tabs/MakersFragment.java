package com.example.nikolaj.minicrmmobile.table.tabs;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.adapters.recyclerview.MakerAdapter;
import com.example.nikolaj.minicrmmobile.cards.MakerCardActivity;
import com.example.nikolaj.minicrmmobile.db.repository.MakerRepository;


/**
 * A simple {@link Fragment} subclass.
 */
public class MakersFragment extends Fragment {

    public MakersFragment() {
        // Required empty public constructor
    }

    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_maker,
                container, false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        return recyclerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getAllMaker();
    }

    public void getAllMaker() {
        MakerRepository makerRepository = new MakerRepository(getContext());
        MakerAdapter makerAdapter = new MakerAdapter(makerRepository.getListMakers());
        recyclerView.setAdapter(makerAdapter);

    }

    public void filterbyId() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_NUMBER);

        builder
                .setView(input)
                .setTitle("Введите ID")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getContext(), MakerCardActivity.class);
                        intent.putExtra(MakerCardActivity.ID, Integer.parseInt(input.getText().toString()));
                        getContext().startActivity(intent);
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    public void filterByName() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final LinearLayout box = new LinearLayout(getContext());
        box.setOrientation(LinearLayout.VERTICAL);

        final EditText inputName = new EditText(getContext());
        final TextView name = new TextView(getContext());
        name.setText("Введите имя");
        box.addView(name);
        box.addView(inputName);

        final EditText inputSurname = new EditText(getContext());
        final TextView surname = new TextView(getContext());
        surname.setText("Введите фамилию");
        box.addView(surname);
        box.addView(inputSurname);

        final EditText inputPatronymic = new EditText(getContext());
        final TextView patronymic = new TextView(getContext());
        patronymic.setText("Введите Отчество");
        box.addView(patronymic);
        box.addView(inputPatronymic);

        builder
                .setView(box)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MakerRepository makerRepository = new MakerRepository(getContext());
                        MakerAdapter makerAdapter = new MakerAdapter(makerRepository.findByName(
                                inputName.getText().toString(), inputSurname.getText().toString(), inputPatronymic.getText().toString()));
                        recyclerView.setAdapter(makerAdapter);
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }
}
