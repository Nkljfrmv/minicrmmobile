package com.example.nikolaj.minicrmmobile.db.pojo;

public class HeadOfSalesDepartment {

    private int id;
    private String name;
    private String surname;
    private String patronymic;
    private String phoneNumber;
    private String eMail;
    private String otherMeans;

    public HeadOfSalesDepartment(int id, String name, String surname, String patronymic, String phoneNumber,
                                 String eMail, String otherMeans) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.phoneNumber = phoneNumber;
        this.eMail = eMail;
        this.otherMeans = otherMeans;

    }

    public HeadOfSalesDepartment(String name, String surname, String patronymic, String phoneNumber,
                                 String eMail, String otherMeans) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.phoneNumber = phoneNumber;
        this.eMail = eMail;
        this.otherMeans = otherMeans;

    }

    public HeadOfSalesDepartment() {
        name = "";
        surname = "";
        patronymic = "";
        phoneNumber = "";
        eMail = "";
        otherMeans = "";
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }
    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEMail() {
        return eMail;
    }
    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

    public String getOtherMeans() {
        return otherMeans;
    }
    public void setOtherMeans(String otherMeans) {
        this.otherMeans = otherMeans;
    }
}
