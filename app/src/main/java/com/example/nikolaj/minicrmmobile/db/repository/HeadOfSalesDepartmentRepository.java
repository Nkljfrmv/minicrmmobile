package com.example.nikolaj.minicrmmobile.db.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.db.DBHelper;
import com.example.nikolaj.minicrmmobile.db.pojo.HeadOfSalesDepartment;

import java.util.ArrayList;
import java.util.List;

public class HeadOfSalesDepartmentRepository {
    private Context context;

    private final String TABLE_NAME = "heads_of_sales_department";

    private String selectId = "SELECT * FROM " + TABLE_NAME + " WHERE  _id=";

    public HeadOfSalesDepartmentRepository(Context context) {
        this.context = context;
    }

    public boolean addHead(HeadOfSalesDepartment head) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();
            ContentValues headValues = new ContentValues();

            headValues.put("name", head.getName());
            headValues.put("surname", head.getSurname());
            headValues.put("patronymic", head.getPatronymic());
            headValues.put("phone_number", head.getPhoneNumber());
            headValues.put("e_mail", head.getEMail());
            headValues.put("other_means", head.getOtherMeans());

            db.insert(TABLE_NAME, null, headValues);
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }


    public boolean updateHead(HeadOfSalesDepartment head) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();
            ContentValues headValues = new ContentValues();

            headValues.put("name", head.getName());
            headValues.put("surname", head.getSurname());
            headValues.put("patronymic", head.getPatronymic());
            headValues.put("phone_number", head.getPhoneNumber());
            headValues.put("e_mail", head.getEMail());
            headValues.put("other_means", head.getOtherMeans());

            db.update(TABLE_NAME,
                    headValues,
                    "_id = ?",
                    new String[] {Integer.toString(head.getId())});
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }

    public boolean deleteHead(HeadOfSalesDepartment head) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();

            db.delete(TABLE_NAME,
                    "_id = ?",
                    new String[] {Integer.toString(head.getId())});
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }

    public HeadOfSalesDepartment findById(int id) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectId+Integer.toString(id)+";", null);
            if (cursor.moveToFirst())
                return getNextHead(cursor);
        }catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }


    private HeadOfSalesDepartment getNextHead(Cursor cursor) {
        int id = cursor.getInt(0);
        String name = cursor.getString(1);
        String surname = cursor.getString(2);
        String patronymic = cursor.getString(3);
        String phoneNumber  = cursor.getString(4);
        String eMail = cursor.getString(5);
        String otherMeans = cursor.getString(6);

        return new HeadOfSalesDepartment(id, name, surname, patronymic, phoneNumber, eMail, otherMeans);
    }
}
