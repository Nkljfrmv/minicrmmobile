package com.example.nikolaj.minicrmmobile.cards;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.databinding.ActivityHeadCardBinding;
import com.example.nikolaj.minicrmmobile.db.pojo.HeadOfSalesDepartment;
import com.example.nikolaj.minicrmmobile.db.repository.HeadOfSalesDepartmentRepository;
import com.example.nikolaj.minicrmmobile.table.TableActivity;

public class HeadCardActivity extends AppCompatActivity {

    private ActivityHeadCardBinding binding;
    private Toolbar toolbar;

    private boolean isNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_head_card);
        binding.setContext(this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        HeadOfSalesDepartment head = new HeadOfSalesDepartmentRepository(this).findById(1);//Тут хардкод, т.к. на данный момент не планируется то, что их будет несколько
        if (head != null) {
            binding.setHead(head);
            isNew = false;
        } else {
            head = new HeadOfSalesDepartment();
            binding.setHead(head);

            isNew = true;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder
                    .setTitle("Внимание")
                    .setMessage("Прежде чем начать пользоваться приложением, добавьте главу отдела продаж")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .show();
        }

    }

    public void onClickFloatingButton(View view) {
        HeadOfSalesDepartmentRepository headRepository = new HeadOfSalesDepartmentRepository(this);
        HeadOfSalesDepartment head = binding.getHead();
        if (isNew) {
            headRepository.addHead(head);
            Intent intent = new Intent(this, TableActivity.class);
            finish();
            startActivity(intent);
        } else {
            headRepository.updateHead(head);
            Snackbar.make(view,"Запись обновлена",Snackbar.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(binding.editId.getText().toString().equals("0")) Snackbar.make(binding.getRoot(),"Добавьте запись о главе отдела продаж",Snackbar.LENGTH_SHORT).show();
            else super.onBackPressed();
    }
}
