package com.example.nikolaj.minicrmmobile.cards;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.nikolaj.minicrmmobile.MainActivity;
import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.databinding.ActivityClientCardBinding;
import com.example.nikolaj.minicrmmobile.db.pojo.Client;
import com.example.nikolaj.minicrmmobile.db.repository.ClientRepository;
import com.example.nikolaj.minicrmmobile.db.repository.DetailActivity;
import com.example.nikolaj.minicrmmobile.db.repository.ManagerRepository;
import com.example.nikolaj.minicrmmobile.db.repository.OrderRepository;

import java.util.List;

public class ClientCardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String ID = "position";

    private DrawerLayout drawer;
    private Toolbar toolbar;
    private FloatingActionButton floatingButton;
    private NavigationView navigation;
    private FrameLayout header;
    private ActivityClientCardBinding binding;

    private boolean isNew;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_client_card);
        binding.setContext(this);

        toolbar = binding.toolbar;
        drawer = binding.drawer;
        navigation = binding.navigationView;
        navigation.setNavigationItemSelectedListener(this);

        header = navigation.getHeaderView(0).findViewById(R.id.header);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                Intent intent = new Intent(ClientCardActivity.this, HeadCardActivity.class);
                startActivity(intent);
            }
        });

        List<Integer> managerIds = new ManagerRepository(this).getIds();
        ArrayAdapter<?> adapter = new ArrayAdapter<>(
               this,
                android.R.layout.simple_spinner_dropdown_item,
                managerIds
        );
        binding.editManagerId.setAdapter(adapter);

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.open_nav_view, R.string.close_nav_view);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.delete:
                        if (binding.getClient().getId() == 0){
                            Snackbar.make(toolbar.getRootView(), "Нет записи", Snackbar.LENGTH_SHORT).show();
                        } else {
                            if (new OrderRepository(toolbar.getContext()).findByIdClient(binding.getClient().getId()).isEmpty()) {
                                new ClientRepository(toolbar.getContext()).deleteClient(binding.getClient().getId());
                                Snackbar.make(toolbar.getRootView(), "Запись удалена", Snackbar.LENGTH_SHORT).show();
                                binding.setClient(new Client());
                                binding.editManagerId.setSelection(0);
                                isNew = true;
                            } else Snackbar.make(toolbar.getRootView(),"Данный клиент имеет заказы",Snackbar.LENGTH_SHORT).show();

                        }
                }
                return false;
            }
        });


        try {
            getIntent().getExtras().getInt(ID);
            isNew = false;
            ClientRepository clientRepository = new ClientRepository(this);
            Client client = clientRepository.findById(getIntent().getExtras().getInt(ID));
            binding.setClient(client);
            binding.editManagerId.setSelection(managerIds.indexOf(client.getManagerId()));
        } catch (Exception e) {
            isNew = true;
            binding.setClient(new Client());
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Intent intent = null;
        onBackPressed();
        if (isNew) Snackbar.make(navigation.getRootView(), "Нет записи",Snackbar.LENGTH_SHORT).show();
            else {
            switch (menuItem.getItemId()) {
                case R.id.to_orders:
                    intent = new Intent(this, DetailActivity.class);
                    intent.putExtra(DetailActivity.ID, binding.getClient().getId());
                    intent.putExtra(DetailActivity.SWITCH, "client_to_orders");
                    startActivity(intent);
                    return true;

                case R.id.to_contact_persons:
                    intent = new Intent(this, DetailActivity.class);
                    intent.putExtra(DetailActivity.ID, binding.getClient().getId());
                    intent.putExtra(DetailActivity.SWITCH, "client_to_persons");
                    startActivity(intent);
                    return true;

                case R.id.manager:
                    intent = new Intent(this, ManagerCardActivity.class);
                    intent.putExtra(ManagerCardActivity.ID, binding.getClient().getManagerId());
                    startActivity(intent);
                    return true;
            }
        }
        if (menuItem.getItemId() == R.id.to_home) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    public void onClickFloatingButton(View view) {
        if (binding.editManagerId.getCount() == 0) {
            Snackbar.make(view,"Проверьте наличие менеджера",Snackbar.LENGTH_SHORT).show();
        } else {
            ClientRepository clientRepository = new ClientRepository(this);
            Client client = binding.getClient();
            client.setManagerId((int) binding.editManagerId.getSelectedItem());
            if (isNew) {
                clientRepository.AddClient(client);
                binding.editId.setText(Integer.toString(clientRepository.getLastId()));
                Snackbar.make(view,"Запись добавлена",Snackbar.LENGTH_SHORT).show();
                isNew = false;

            } else {
                clientRepository.updateClient(client);
                Snackbar.make(view,"Запись обновлена",Snackbar.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

}
