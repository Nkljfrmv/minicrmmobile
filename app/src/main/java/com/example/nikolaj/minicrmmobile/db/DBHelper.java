package com.example.nikolaj.minicrmmobile.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static final String DB_NAME = "mydb";
    public static final int DB_VERSION = 1;

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys=on;");
        db.execSQL("CREATE TABLE heads_of_sales_department(" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "name TEXT NOT NULL," +
                "surname TEXT NOT NULL," +
                "patronymic TEXT," +
                "phone_number TEXT," +
                "e_mail TEXT," +
                "other_means TEXT);");

        db.execSQL("CREATE TABLE makers(" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "name TEXT NOT NULL," +
                "surname TEXT NOT NULL," +
                "patronymic TEXT," +
                "phone_number TEXT," +
                "e_mail TEXT," +
                "other_means TEXT);");

        db.execSQL("CREATE TABLE managers(" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "name TEXT NOT NULL," +
                "surname TEXT NOT NULL," +
                "patronymic TEXT," +
                "phone_number TEXT," +
                "e_mail TEXT," +
                "other_means TEXT," +
                "head_id INTEGER NOT NULL," +
                "FOREIGN KEY (head_id) REFERENCES heads_of_sales_department(_id));");

        db.execSQL("CREATE TABLE clients(" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "status TEXT NOT NULL," +
                "type TEXT NOT NULL," +
                "log TEXT," +
                "notes TEXT," +
                "manager_id INTEGER NOT NULL," +
                "FOREIGN KEY (manager_id) REFERENCES managers(_id));");

        db.execSQL("CREATE TABLE contact_persons(" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "name TEXT NOT NULL," +
                "surname TEXT NOT NULL," +
                "patronymic TEXT," +
                "phone_number TEXT," +
                "e_mail TEXT," +
                "other_means TEXT," +
                "position TEXT," +
                "notes TEXT," +
                "client_id INTEGER NOT NULL," +
                "FOREIGN KEY (client_id) REFERENCES clients(_id));");

        db.execSQL("CREATE TABLE orders(" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "order_type INTEGER NOT NULL," +
                "add_options TEXT ," +
                "order_status Integer NOT NULL," +
                "date_start TEXT," +
                "date_finish TEXT," +
                "log TEXT," +
                "client_id INTEGER NOT NULL," +
                "maker_id INTEGER NOT NULL," +
                "FOREIGN KEY (client_id) REFERENCES clients(_id)," +
                "FOREIGN KEY (maker_id) REFERENCES makers(_id));");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
