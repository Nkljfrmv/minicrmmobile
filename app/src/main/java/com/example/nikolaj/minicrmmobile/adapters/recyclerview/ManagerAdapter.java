package com.example.nikolaj.minicrmmobile.adapters.recyclerview;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.cards.ManagerCardActivity;
import com.example.nikolaj.minicrmmobile.databinding.TableItemManagerBinding;
import com.example.nikolaj.minicrmmobile.db.pojo.Manager;

import java.util.List;

public class ManagerAdapter extends RecyclerView.Adapter<ManagerAdapter.ViewHolder>{

    private List<Manager> managers;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TableItemManagerBinding binding;

        public ViewHolder(TableItemManagerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
        public void bind(Manager manager) {
            binding.setManager(manager);
            binding.executePendingBindings();
        }
    }

    public ManagerAdapter(List<Manager> managers) {
        this.managers = managers;
    }

    @Override
    public int getItemCount() {
        return managers.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        TableItemManagerBinding binding = DataBindingUtil.inflate(inflater,R.layout.table_item_manager,parent, false);
        return new ViewHolder(binding);
    }

    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.bind(managers.get(position));
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ManagerCardActivity.class);
                intent.putExtra(ManagerCardActivity.ID, managers.get(position).getId());
                v.getContext().startActivity(intent);
            }
        });

    }
}
