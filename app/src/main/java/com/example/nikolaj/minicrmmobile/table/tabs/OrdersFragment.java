package com.example.nikolaj.minicrmmobile.table.tabs;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.adapters.recyclerview.OrderAdapter;
import com.example.nikolaj.minicrmmobile.db.repository.DetailActivity;
import com.example.nikolaj.minicrmmobile.cards.OrderCardActivity;
import com.example.nikolaj.minicrmmobile.db.repository.ClientRepository;
import com.example.nikolaj.minicrmmobile.db.repository.MakerRepository;
import com.example.nikolaj.minicrmmobile.db.repository.OrderRepository;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrdersFragment extends Fragment {

    private RecyclerView recyclerView;

    public OrdersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_order,
                container, false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        return recyclerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            switch (getActivity().getIntent().getExtras().getString(DetailActivity.SWITCH)) {
                case "client_to_orders":
                    filterByIdClient(getActivity().getIntent().getExtras().getInt(DetailActivity.ID));
                    break;

                case "maker_to_orders":
                    filterByIdMaker(getActivity().getIntent().getExtras().getInt(DetailActivity.ID));
            }
        } catch (Exception e) {
            getAllOrders();
        }
    }

    public void getAllOrders() {
        OrderRepository orderRepository = new OrderRepository(getContext());
        OrderAdapter adapter = new OrderAdapter(orderRepository.getListOrders());
        recyclerView.setAdapter(adapter);
    }

    public void filterById() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_NUMBER);

        builder
                .setView(input)
                .setTitle("Введите ID")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getContext(), OrderCardActivity.class);
                        intent.putExtra(OrderCardActivity.ID, Integer.parseInt(input.getText().toString()));
                        getContext().startActivity(intent);
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    public void filterByStatus() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final Spinner status = new Spinner(getContext());
        ArrayAdapter<?> adapterStatus = ArrayAdapter.createFromResource(
                getContext(),
                R.array.order_status,
                android.R.layout.simple_spinner_dropdown_item
        );
        status.setAdapter(adapterStatus);

        builder
                .setView(status)
                .setTitle("Выберете статус")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        OrderRepository orderRepository = new OrderRepository(getContext());
                        OrderAdapter adapter = new OrderAdapter(orderRepository.findByStatus(status.getSelectedItemPosition()));
                        recyclerView.setAdapter(adapter);

                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    public void filterByType() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final Spinner type = new Spinner(getContext());
        ArrayAdapter<?> adapterType = ArrayAdapter.createFromResource(
                getContext(),
                R.array.order_type,
                android.R.layout.simple_spinner_dropdown_item
        );
        type.setAdapter(adapterType);

        builder
                .setView(type)
                .setTitle("Выберете статус")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        OrderRepository orderRepository = new OrderRepository(getContext());
                        OrderAdapter adapter = new OrderAdapter(orderRepository.findByType(type.getSelectedItemPosition()));
                        recyclerView.setAdapter(adapter);
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    public void filterByIdMaker() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final Spinner makerId = new Spinner(getContext());
        ArrayAdapter<?> adapterMakerIds = new ArrayAdapter<>(
                getContext(),
                android.R.layout.simple_spinner_dropdown_item,
                new MakerRepository(getContext()).getIds());
        makerId.setAdapter(adapterMakerIds);

        builder
                .setView(makerId)
                .setTitle("Выберете Id Разработчика")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        OrderRepository orderRepository = new OrderRepository(getContext());
                        OrderAdapter adapter = new OrderAdapter(orderRepository.findByIdMaker((int)makerId.getSelectedItem()));
                        recyclerView.setAdapter(adapter);
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    public void filterByIdMaker(int id) {
        OrderRepository orderRepository = new OrderRepository(getContext());
        OrderAdapter adapter = new OrderAdapter(orderRepository.findByIdMaker(id));
        recyclerView.setAdapter(adapter);
    }

    public void filterByIdClient() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final Spinner clientId = new Spinner(getContext());
        ArrayAdapter<?> adapterClientIds = new ArrayAdapter<>(
                getContext(),
                android.R.layout.simple_spinner_dropdown_item,
                new ClientRepository(getContext()).getIds());
        clientId.setAdapter(adapterClientIds);

        builder
                .setView(clientId)
                .setTitle("Выберете Id клиента")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        OrderRepository orderRepository = new OrderRepository(getContext());
                        OrderAdapter adapter = new OrderAdapter(orderRepository.findByIdClient((int)clientId.getSelectedItem()));
                        recyclerView.setAdapter(adapter);
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();


    }

    public void filterByIdClient(int id) {
        OrderRepository orderRepository = new OrderRepository(getContext());
        OrderAdapter adapter = new OrderAdapter(orderRepository.findByIdClient(id));
        recyclerView.setAdapter(adapter);
    }



}
