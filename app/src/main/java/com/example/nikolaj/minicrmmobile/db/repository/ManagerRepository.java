package com.example.nikolaj.minicrmmobile.db.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.db.DBHelper;
import com.example.nikolaj.minicrmmobile.db.pojo.Manager;

import java.util.ArrayList;
import java.util.List;

public class ManagerRepository {
    private Context context;

    private final String TABLE_NAME = "managers";

    private String selectList = "SELECT * FROM " + TABLE_NAME + ";";
    private String selectId = "SELECT * FROM "+ TABLE_NAME +" WHERE _id=";
    private String selectName =
            "SELECT * FROM managers WHERE " +
                    "CASE ? WHEN '' THEN name LIKE '%' ELSE name LIKE ? END" +
                    " AND " +
                    "CASE ? WHEN '' THEN surname LIKE '%' ELSE surname LIKE ? END" +
                    " AND " +
                    "CASE ? WHEN '' THEN patronymic LIKE '%' ELSE patronymic LIKE ? END;";
    private String selectLastId = "SELECT _id FROM " + TABLE_NAME + " ORDER BY _id DESC LIMIT 1";

    public ManagerRepository(Context context) {
        this.context = context;
    }

    public boolean addManager(Manager manager) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();
            ContentValues managerValues = new ContentValues();
            managerValues.put("head_id", manager.getHeadId());
            managerValues.put("name", manager.getName());
            managerValues.put("surname", manager.getSurname());
            managerValues.put("patronymic", manager.getPatronymic());
            managerValues.put("phone_number", manager.getPhoneNumber());
            managerValues.put("e_mail", manager.getEMail());
            managerValues.put("other_means", manager.getOtherMeans());

            db.insert(TABLE_NAME, null, managerValues);
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }


    public boolean updateManager(Manager manager) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();
            ContentValues managerValues = new ContentValues();

            managerValues.put("name", manager.getName());
            managerValues.put("surname", manager.getSurname());
            managerValues.put("patronymic", manager.getPatronymic());
            managerValues.put("phone_number", manager.getPhoneNumber());
            managerValues.put("e_mail", manager.getEMail());
            managerValues.put("other_means", manager.getOtherMeans());

            db.update(TABLE_NAME,
                    managerValues,
                    "_id = ?",
                    new String[]{Integer.toString(manager.getId())});
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }

    public boolean deleteManager(int id) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();

            db.delete(TABLE_NAME,
                    "_id = ?",
                    new String[]{Integer.toString(id)});
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }

    public Manager findById(int id) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectId + Integer.toString(id) + ";", null);
            if (cursor.moveToFirst())
                return getNextMaker(cursor);
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }

    public List<Manager> getListManager() {
        return getListByQuery(selectList);
    }


    public List<Manager> findByName(String name, String surname, String patronymic) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectName, new String[]{name, name, surname, surname, patronymic, patronymic});

            List<Manager> managers = new ArrayList<>();
            if (cursor.moveToFirst())
                for (int i = 0; i < cursor.getCount(); i++) {
                    managers.add(getNextMaker(cursor));
                    cursor.moveToNext();
                }
            return managers;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }


    public List<Integer> getIds() {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT _id FROM " +TABLE_NAME +";", null);
            List<Integer> ids = new ArrayList<>();
            if (cursor.moveToFirst());
            for (int i = 0; i < cursor.getCount(); i++){
                ids.add(cursor.getInt(0));
                cursor.moveToNext();
            }
            return ids;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }

    public int getLastId() {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectLastId, null);
            if (cursor.moveToFirst());
                return cursor.getInt(0);
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return -1;
    }

    private List<Manager> getListByQuery(String sql) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            List<Manager> managers = new ArrayList<>();
            if (cursor.moveToFirst())
                for (int i = 0; i < cursor.getCount(); i++) {
                    managers.add(getNextMaker(cursor));
                    cursor.moveToNext();
                }
            return managers;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }

    private Manager getNextMaker(Cursor cursor) {
        int id = cursor.getInt(0);
        int headId = cursor.getInt(7);
        String name = cursor.getString(1);
        String surname = cursor.getString(2);
        String patronymic = cursor.getString(3);
        String phoneNumber = cursor.getString(4);
        String eMail = cursor.getString(5);
        String otherMeans = cursor.getString(6);

        return new Manager(id, headId, name, surname, patronymic, phoneNumber, eMail, otherMeans);
    }

}
