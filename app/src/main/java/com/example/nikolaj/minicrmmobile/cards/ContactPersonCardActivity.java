package com.example.nikolaj.minicrmmobile.cards;

import android.databinding.DataBindingUtil;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.databinding.ActivityContactPersonCardBinding;
import com.example.nikolaj.minicrmmobile.db.pojo.ContactPerson;
import com.example.nikolaj.minicrmmobile.db.repository.ContactPersonRepository;
import com.example.nikolaj.minicrmmobile.db.repository.DetailActivity;

public class ContactPersonCardActivity extends AppCompatActivity {

    public static final String ID = "position";

    private Toolbar toolbar;
    private ActivityContactPersonCardBinding binding;

    private boolean isNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contact_person_card);
        binding.setContext(this);

        toolbar = binding.toolbar;
        setSupportActionBar(toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.delete:
                        if (binding.getPerson().getId() == 0){
                            Snackbar.make(toolbar.getRootView(), "Нет записи", Snackbar.LENGTH_SHORT).show();

                        } else {
                            new ContactPersonRepository(toolbar.getContext()).deleteContactPerson(binding.getPerson().getId());
                            Snackbar.make(toolbar.getRootView(), "Запись удалена", Snackbar.LENGTH_SHORT).show();
                            binding.setPerson(new ContactPerson());
                            isNew = true;
                        }
                }
                return false;
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

            if (getIntent().getExtras().getInt(ID) == 0) { //Проверить интент
                isNew = true;
                ContactPerson person = new ContactPerson();
                person.setClientId(getIntent().getExtras().getInt(DetailActivity.ID));
                binding.setPerson(person);
            } else {
            isNew = false;
            ContactPersonRepository contactPersonRepository = new ContactPersonRepository(this);
            binding.setPerson(contactPersonRepository.findById(getIntent().getExtras().getInt(ID)));
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }
    public void onClickFloatingButton(View view) {
        ContactPersonRepository contactPersonRepository = new ContactPersonRepository(this);
        ContactPerson person = binding.getPerson();
        if (isNew) {
            contactPersonRepository.addContactPerson(person);
            binding.editId.setText(Integer.toString(contactPersonRepository.getLastId()));
            Snackbar.make(view,"Запись добавлена",Snackbar.LENGTH_SHORT).show();
            isNew = false;
        } else {
            contactPersonRepository.updateContactPerson(person);
            Snackbar.make(view,"Запись обновлена",Snackbar.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

}
