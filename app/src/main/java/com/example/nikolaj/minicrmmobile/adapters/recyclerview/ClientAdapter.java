package com.example.nikolaj.minicrmmobile.adapters.recyclerview;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.cards.ClientCardActivity;
import com.example.nikolaj.minicrmmobile.databinding.TableItemClientBinding;
import com.example.nikolaj.minicrmmobile.db.pojo.Client;

import java.util.List;

public class ClientAdapter extends RecyclerView.Adapter<ClientAdapter.ViewHolder>{

    private List<Client> clients;

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TableItemClientBinding binding;

        public ViewHolder(TableItemClientBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
        public void bind(Client client) {
            binding.setClient(client);
            binding.typeVal.setText(binding.getRoot().getResources().getStringArray(R.array.client_type)[client.getType()]);
            binding.statusVal.setText(binding.getRoot().getResources().getStringArray(R.array.client_status)[client.getStatus()]);
            binding.executePendingBindings();
        }
    }

    public ClientAdapter(List<Client> clients) {
        this.clients = clients;
    }

    @Override
    public int getItemCount() {
        return clients.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        TableItemClientBinding binding = DataBindingUtil.inflate(inflater, R.layout.table_item_client, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.bind(clients.get(position));
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ClientCardActivity.class);
                intent.putExtra(ClientCardActivity.ID, clients.get(position).getId());
                v.getContext().startActivity(intent);
            }
        });
    }
}
