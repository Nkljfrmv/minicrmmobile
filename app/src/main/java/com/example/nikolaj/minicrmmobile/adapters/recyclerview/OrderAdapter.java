package com.example.nikolaj.minicrmmobile.adapters.recyclerview;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.cards.OrderCardActivity;
import com.example.nikolaj.minicrmmobile.databinding.TableItemOrderBinding;
import com.example.nikolaj.minicrmmobile.db.pojo.Order;

import java.util.List;


public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder>{

    private List<Order> orders;

    public static  class ViewHolder extends RecyclerView.ViewHolder{
        private TableItemOrderBinding binding;

        public ViewHolder(TableItemOrderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Order order) {
            binding.setOrder(order);

            binding.typeVal.setText(binding.getRoot().getResources().getStringArray(R.array.order_type)[order.getType()]);
            binding.statusVal.setText(binding.getRoot().getResources().getStringArray(R.array.order_status)[order.getStatus()]);
            binding.executePendingBindings();
        }
    }

    public OrderAdapter(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        TableItemOrderBinding binding = DataBindingUtil.inflate(inflater, R.layout.table_item_order, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.bind(orders.get(position));
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), OrderCardActivity.class);
                intent.putExtra(OrderCardActivity.ID, orders.get(position).getId());
                v.getContext().startActivity(intent);
            }
        });

    }
}
