package com.example.nikolaj.minicrmmobile.cards;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.nikolaj.minicrmmobile.MainActivity;
import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.databinding.ActivityManagerCardBinding;
import com.example.nikolaj.minicrmmobile.db.pojo.Manager;
import com.example.nikolaj.minicrmmobile.db.repository.ClientRepository;
import com.example.nikolaj.minicrmmobile.db.repository.DetailActivity;
import com.example.nikolaj.minicrmmobile.db.repository.ManagerRepository;

public class ManagerCardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ActivityManagerCardBinding binding;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigation;
    private FrameLayout header;

    public static final String ID ="position";

    private boolean isNew;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_manager_card);
        binding.setContext(this);
        toolbar = binding.toolbar;
        drawer = binding.drawer;
        navigation = binding.navigationView;
        navigation.setNavigationItemSelectedListener(this);

        header = navigation.getHeaderView(0).findViewById(R.id.header);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManagerCardActivity.this, HeadCardActivity.class);
                startActivity(intent);
            }
        });

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.open_nav_view, R.string.close_nav_view);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        try {
            getIntent().getExtras().getInt(ID);
            isNew = false;
            ManagerRepository managerRepository = new ManagerRepository(this);
            binding.setManager(managerRepository.findById(getIntent().getExtras().getInt(ID)));
        } catch (Exception e) {
            binding.setManager(new Manager());
            isNew = true;
        }



        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.delete:
                        if (binding.getManager().getId() == 0){
                            Snackbar.make(toolbar.getRootView(), "Нет записи", Snackbar.LENGTH_SHORT).show(); } else {
                            if(new ClientRepository(toolbar.getContext()).findByIdManager(binding.getManager().getId()).isEmpty()) {
                                new ManagerRepository(toolbar.getContext()).deleteManager(binding.getManager().getId());
                                Snackbar.make(toolbar.getRootView(), "Запись удалена", Snackbar.LENGTH_SHORT).show();
                                binding.setManager(new Manager());
                                isNew = true;
                            } else Snackbar.make(toolbar.getRootView(),"Данный менеджер имеет клиентов",Snackbar.LENGTH_SHORT).show();
                        }
                    }
                return false;
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        onBackPressed();
        Intent intent;
        if (isNew) Snackbar.make(toolbar.getRootView(), "Нет записи", Snackbar.LENGTH_SHORT).show();
        else if(menuItem.getItemId() == R.id.to_clients) {
            intent = new Intent(this, DetailActivity.class);
            intent.putExtra(DetailActivity.ID, binding.getManager().getId());
            intent.putExtra(DetailActivity.SWITCH, "manager_to_clients");
            startActivity(intent);
            return true;
        }
        if (menuItem.getItemId() == R.id.to_home) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    public void onClickFloatingButton(View view) {
        ManagerRepository managerRepository = new ManagerRepository(this);
        Manager manager = binding.getManager();
        if (isNew) {
            managerRepository.addManager(manager);
            binding.editId.setText(Integer.toString(managerRepository.getLastId()));
            Snackbar.make(view,"Запись добавлена",Snackbar.LENGTH_SHORT).show();
            isNew = false;
        } else {
            managerRepository.updateManager(manager);
            Snackbar.make(view,"Запись обновлена",Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

}
