package com.example.nikolaj.minicrmmobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.nikolaj.minicrmmobile.cards.HeadCardActivity;
import com.example.nikolaj.minicrmmobile.db.repository.ClientRepository;
import com.example.nikolaj.minicrmmobile.db.repository.HeadOfSalesDepartmentRepository;
import com.example.nikolaj.minicrmmobile.db.repository.OrderRepository;
import com.example.nikolaj.minicrmmobile.table.TableActivity;

public class MainActivity extends AppCompatActivity {

    private TextView numOpenTasks;
    private TextView numReadyTasks;
    private TextView numPotentialClients;

    Button start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        start = findViewById(R.id.start);

        numOpenTasks = findViewById(R.id.num_open_tasks);
        numReadyTasks = findViewById(R.id.num_ready_tasks);
        numPotentialClients = findViewById(R.id.num_potential_clients);

        numOpenTasks.setText(Integer.toString(new OrderRepository(this).findByStatus(1).size()));
        numReadyTasks.setText(Integer.toString(new OrderRepository(this).findByStatus(3).size()));
        numPotentialClients.setText(Integer.toString(new ClientRepository(this).findByStatus(1).size()));

        HeadOfSalesDepartmentRepository headRepository = new HeadOfSalesDepartmentRepository(this);
        if (headRepository.findById(1) == null ) {
            Intent intent = new Intent(this, HeadCardActivity.class);
            startActivity(intent);
        }
    }

    public void toTable(View v) {
        Intent intent = new Intent(this, TableActivity.class);
        startActivity(intent);
    }
}
