package com.example.nikolaj.minicrmmobile.adapters.recyclerview;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.cards.ContactPersonCardActivity;
import com.example.nikolaj.minicrmmobile.databinding.TableItemContactPersonBinding;
import com.example.nikolaj.minicrmmobile.db.pojo.ContactPerson;

import java.util.List;

public class ContactPersonAdapter extends RecyclerView.Adapter<ContactPersonAdapter.ViewHolder> {

    List<ContactPerson> persons;

    public ContactPersonAdapter(List<ContactPerson> persons) {
        this.persons = persons;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TableItemContactPersonBinding binding;

        public ViewHolder(TableItemContactPersonBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(ContactPerson person) {
            binding.setPerson(person);
            binding.executePendingBindings();
        }
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        TableItemContactPersonBinding binding = DataBindingUtil.inflate(inflater, R.layout.table_item_contact_person, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.bind(persons.get(position));
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ContactPersonCardActivity.class);
                intent.putExtra(ContactPersonCardActivity.ID, persons.get(position).getId());
                v.getContext().startActivity(intent);
            }
        });
    }
}
