package com.example.nikolaj.minicrmmobile.db.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.db.DBHelper;
import com.example.nikolaj.minicrmmobile.db.pojo.Order;

import java.util.ArrayList;
import java.util.List;

public class OrderRepository {

    private Context context;

    private final String TABLE_NAME = "orders";

    private String selectList = "SELECT * FROM " + TABLE_NAME +";";
    private String selectId = "SELECT * FROM " + TABLE_NAME +" WHERE _id = ";
    private String selectIdClient= "SELECT * FROM " + TABLE_NAME +" WHERE client_id =";
    private String selectIdMaker = "SELECT * FROM " + TABLE_NAME +" WHERE maker_id = ";
    private String selectType = "SELECT * FROM " + TABLE_NAME +" WHERE order_type =";
    private String selectOrderStatus= "SELECT * FROM " + TABLE_NAME +" WHERE order_status =";
    private String selectLastId = "SELECT _id FROM " + TABLE_NAME + " ORDER BY _id DESC LIMIT 1";

    public OrderRepository(Context context) {
        this.context = context;
    }

    public boolean addOrder(Order order){
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;

        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();
            ContentValues orderValues = new ContentValues();

            orderValues.put("order_type", order.getType());
            orderValues.put("add_options", order.getAddOptions());
            orderValues.put("order_status", order.getStatus());
            orderValues.put("date_start", order.getDateStart());
            orderValues.put("date_finish", order.getDateFinish());
            orderValues.put("log", order.getLog());
            orderValues.put("client_id", order.getClientId());
            orderValues.put("maker_id", order.getMakerId());

            db.insert(TABLE_NAME, null, orderValues);
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }

    public boolean udpateOrder(Order order) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();
            ContentValues orderValues = new ContentValues();

            orderValues.put("order_type", order.getType());
            orderValues.put("add_options", order.getAddOptions());
            orderValues.put("order_status", order.getStatus());
            orderValues.put("date_start", order.getDateStart());
            orderValues.put("date_finish", order.getDateFinish());
            orderValues.put("log", order.getLog());
            orderValues.put("client_id", order.getClientId());
            orderValues.put("maker_id", order.getMakerId());

            db.update(TABLE_NAME,
                    orderValues,
                    "_id = ?",
            new String[] {Integer.toString(order.getId())});
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }

    public boolean deleteOrder(int id) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();

            db.delete(TABLE_NAME,
                    "_id = ?",
                    new String[] {Integer.toString(id)});
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }


    public Order findById(int id) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectId+Integer.toString(id),null);
            if (cursor.moveToFirst())
                return getNextOrder(cursor);
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }

    public List<Order> getListOrders() {
        return getListByQuery(selectList);
    }

    public List<Order> findByIdClient(int clientId) {
        return getListByQuery(selectIdClient + "\"" + Integer.toString(clientId) + "\";");
    }

    public List<Order> findByIdMaker(int makerId) {
        return getListByQuery(selectIdMaker + "\"" + Integer.toString(makerId) + "\";");
    }

    public List<Order> findByStatus(int status) {
        return getListByQuery(selectOrderStatus + "\"" + Integer.toString(status) + "\";");
    }

    public List<Order> findByType(int type) {
        return getListByQuery(selectType + "\"" + Integer.toString(type) + "\";");
    }

    public List<Integer> getIds() {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT _id FROM " +TABLE_NAME +";", null);
            List<Integer> ids = new ArrayList<>();
            if (cursor.moveToFirst());
            for (int i = 0; i < cursor.getCount(); i++){
                ids.add(cursor.getInt(0));
                cursor.moveToNext();
            }
            return ids;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }

    public int getLastId() {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectLastId, null);
            if (cursor.moveToFirst());
                return cursor.getInt(0);
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return -1;
    }


    private List<Order> getListByQuery(String sql) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            List<Order> orders = new ArrayList<>();
            if (cursor.moveToFirst()) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    orders.add(getNextOrder(cursor));
                    cursor.moveToNext();
                }
            }

            return orders;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }

    private Order getNextOrder(Cursor cursor) {

        int id = cursor.getInt(0);
        int type = cursor.getInt(1);
        String addOptions= cursor.getString(2);
        int status = cursor.getInt(3);
        String dateStart = cursor.getString(4);
        String dateFinish = cursor.getString(5);
        String log = cursor.getString(6);
        int clientId = cursor.getInt(7);
        int makerId = cursor.getInt(8);

        return new Order(id, clientId, makerId, type, addOptions, status, dateStart, dateFinish, log);
    }

}
