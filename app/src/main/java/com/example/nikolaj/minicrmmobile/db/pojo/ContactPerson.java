package com.example.nikolaj.minicrmmobile.db.pojo;

public class ContactPerson {

    private int id;
    private int clientId;
    private String name;
    private String surname;
    private String patronymic;
    private String phoneNumber;
    private String eMail;
    private String otherMeans;
    private String position;
    private String notes;

    public ContactPerson(int id, int clientId, String name, String surname, String patronymic,
                         String phoneNumber, String eMail, String otherMeans, String position, String notes){
        this.id = id;
        this.clientId = clientId;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.phoneNumber = phoneNumber;
        this.eMail = eMail;
        this.otherMeans = otherMeans;
        this.position = position;
        this.notes = notes;

    }

    public ContactPerson(int clientId, String name, String surname, String patronymic,
                         String phoneNumber, String eMail, String otherMeans, String position, String notes){
        this.clientId = clientId;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.phoneNumber = phoneNumber;
        this.eMail = eMail;
        this.otherMeans = otherMeans;
        this.position = position;
        this.notes = notes;

    }

    public ContactPerson() {
        name = "";
        surname = "";
        patronymic = "";
        phoneNumber = "";
        eMail = "";
        otherMeans = "";
        position = "";
        notes = "";
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }
    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }
    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEMail() {
        return eMail;
    }
    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

    public String getOtherMeans() {
        return otherMeans;
    }
    public void setOtherMeans(String otherMeans) {
        this.otherMeans = otherMeans;
    }

    public String getPosition() {
        return position;
    }
    public void setPosition(String position) {
        this.position = position;
    }

    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }
}
