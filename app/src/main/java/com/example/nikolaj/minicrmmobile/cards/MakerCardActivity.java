package com.example.nikolaj.minicrmmobile.cards;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.nikolaj.minicrmmobile.MainActivity;
import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.databinding.ActivityMakerCardBinding;
import com.example.nikolaj.minicrmmobile.db.pojo.Maker;
import com.example.nikolaj.minicrmmobile.db.repository.DetailActivity;
import com.example.nikolaj.minicrmmobile.db.repository.MakerRepository;
import com.example.nikolaj.minicrmmobile.db.repository.OrderRepository;

public class MakerCardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String ID = "position";

    private ActivityMakerCardBinding binding;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigation;
    private FrameLayout header;

    private boolean isNew;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maker_card);
        binding.setContext(this);

        toolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer);
        navigation = findViewById(R.id.navigation_view);
        navigation.setNavigationItemSelectedListener(this);
        setSupportActionBar(toolbar);

        header = navigation.getHeaderView(0).findViewById(R.id.header);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                Intent intent = new Intent(MakerCardActivity.this, HeadCardActivity.class);
                startActivity(intent);
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.open_nav_view, R.string.close_nav_view);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        try {
            getIntent().getExtras().getInt(ID);
            isNew = false;
            MakerRepository makerRepository = new MakerRepository(this);
            binding.setMaker(makerRepository.findById(getIntent().getExtras().getInt(ID)));
        } catch (Exception e) {
            isNew = true;
            binding.setMaker(new Maker());
        }
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.delete:
                        if (binding.editId.getText().toString().equals("0")){
                            Snackbar.make(toolbar.getRootView(), "Нет записи", Snackbar.LENGTH_SHORT).show();

                        } else {
                            if (new OrderRepository(toolbar.getContext()).findByIdMaker(binding.getMaker().getId()).isEmpty()) {
                                new MakerRepository(toolbar.getContext()).deleteMaker(binding.getMaker().getId());
                                Snackbar.make(toolbar.getRootView(), "Запись удалена", Snackbar.LENGTH_SHORT).show();
                                binding.setMaker(new Maker());
                                isNew = true;
                            }
                            else Snackbar.make(toolbar.getRootView(),"Данный разработчик задействован в заказае",Snackbar.LENGTH_SHORT).show();
                        }
                }
                return false;
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Intent intent;
        onBackPressed();
        if (isNew) Snackbar.make(toolbar.getRootView(), "Нет записи", Snackbar.LENGTH_SHORT).show();
        else if (menuItem.getItemId() == R.id.to_orders) {
            intent = new Intent(this, DetailActivity.class);
            intent.putExtra(DetailActivity.SWITCH, "maker_to_orders");
            intent.putExtra(DetailActivity.ID, binding.getMaker().getId());
            startActivity(intent);
            return true;
        }
        if (menuItem.getItemId() == R.id.to_home) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    public void onClickFloatingButton(View view) {
       MakerRepository makerRepository = new MakerRepository(this);
        if (isNew) {
            makerRepository.addMaker(binding.getMaker());
            binding.editId.setText(Integer.toString(makerRepository.getLastId()));
            Snackbar.make(view,"Запись добавлена",Snackbar.LENGTH_SHORT).show();
            isNew = false;

        } else {
            makerRepository.updateMaker(binding.getMaker());
            Snackbar.make(view,"Запись обновлена",Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
