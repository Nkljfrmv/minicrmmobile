package com.example.nikolaj.minicrmmobile.db.pojo;

public class Order {

    private int id;
    private int clientId;
    private int makerId;
    private int type;
    private String addOptions;
    private int status;
    private String dateStart;
    private String dateFinish;
    private String log;

    public Order(int id, int clientId, int makerId, int type, String addOptions, int status,
                 String dateStart, String dateFinish, String log) {
        this.id = id;
        this.clientId = clientId;
        this.makerId = makerId;
        this.type = type;
        this.addOptions = addOptions;
        this.status = status;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        this.log = log;

    }

    public Order(int clientId, int makerId, int type, String addOptions, int status,
                 String dateStart, String dateFinish, String log) {
        this.clientId = clientId;
        this.makerId = makerId;
        this.type = type;
        this.addOptions = addOptions;
        this.status = status;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        this.log = log;

    }

    public Order() {
        type = 0;
        addOptions = "";
        status = 0;
        dateStart = "";
        dateFinish = "";
        log = "";
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }
    public void setIdClient(int clientId) {
        this.clientId = clientId;
    }

    public int getMakerId() {
        return makerId;
    }
    public void setIdMaker(int MakerId) {
        this.makerId = MakerId;
    }

    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }

    public String getAddOptions() {
        return addOptions;
    }
    public void setAddOptions(String addOptions) {
        this.addOptions = addOptions;
    }

    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }

    public String getDateStart() {
        return dateStart;
    }
    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateFinish() {
        return dateFinish;
    }
    public void setDateFinish(String dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getLog() {
        return log;
    }
    public void setLog(String log) {
        this.log = log;
    }
}
