package com.example.nikolaj.minicrmmobile.db.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.nikolaj.minicrmmobile.R;
import com.example.nikolaj.minicrmmobile.db.DBHelper;
import com.example.nikolaj.minicrmmobile.db.pojo.Maker;

import java.util.ArrayList;
import java.util.List;

public class MakerRepository {

    private Context context;

    private final String TABLE_NAME = "makers";

    private String selectList = "SELECT * FROM " + TABLE_NAME +";";
    private String selectId = "SELECT * FROM " + TABLE_NAME + " WHERE _id=";
    private String selectName =
            "SELECT * FROM " + TABLE_NAME + " WHERE " +
                    "CASE ? WHEN '' THEN name LIKE '%' ELSE name LIKE ? END" +
                    " AND " +
                    "CASE ? WHEN '' THEN surname LIKE '%' ELSE surname LIKE ? END" +
                    " AND " +
                    "CASE ? WHEN '' THEN patronymic LIKE '%' ELSE patronymic LIKE ? END;";
    private String selectLastId = "SELECT _id FROM " + TABLE_NAME + " ORDER BY _id DESC LIMIT 1";

    public MakerRepository(Context context) {
        this.context = context;
    }

    public boolean addMaker(Maker maker) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();
            ContentValues makerValues = new ContentValues();

            makerValues.put("name", maker.getName());
            makerValues.put("surname", maker.getSurname());
            makerValues.put("patronymic", maker.getPatronymic());
            makerValues.put("phone_number", maker.getPhoneNumber());
            makerValues.put("e_mail", maker.getEMail());
            makerValues.put("other_means", maker.getOtherMeans());

            db.insert(TABLE_NAME, null, makerValues);
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }


    public boolean updateMaker(Maker maker) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();
            ContentValues makerValues = new ContentValues();

            makerValues.put("name", maker.getName());
            makerValues.put("surname", maker.getSurname());
            makerValues.put("patronymic", maker.getPatronymic());
            makerValues.put("phone_number", maker.getPhoneNumber());
            makerValues.put("e_mail", maker.getEMail());
            makerValues.put("other_means", maker.getOtherMeans());

            db.update(TABLE_NAME,
                    makerValues,
                    "_id = ?",
                    new String[] {Integer.toString(maker.getId())});
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }

    public boolean deleteMaker(int id) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getWritableDatabase();

            db.delete(TABLE_NAME,
                    "_id = ?",
                    new String[] {Integer.toString(id)});
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return false;
    }

    public Maker findById(int id) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectId+Integer.toString(id)+";", null);
            if (cursor.moveToFirst())
                return getNextMaker(cursor);
        }catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }

    public List<Maker> getListMakers() {
        return getListByQuery(selectList);
    }


    public List<Maker> findByName(String name, String surname, String patronymic) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectName, new String[]{name, name, surname, surname, patronymic, patronymic});

            List<Maker> makers = new ArrayList<>();

            if (cursor.moveToFirst()) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    makers.add(getNextMaker(cursor));
                    cursor.moveToNext();
                }
            }
            return makers;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }


    public List<Integer> getIds() {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT _id FROM " +TABLE_NAME +";", null);
            List<Integer> ids = new ArrayList<>();
            if (cursor.moveToFirst());
            for (int i = 0; i < cursor.getCount(); i++){
                ids.add(cursor.getInt(0));
                cursor.moveToNext();
            }
            return ids;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }

    public int getLastId() {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectLastId, null);
            if (cursor.moveToFirst()) {
                return cursor.getInt(0);
            }
        }catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return -1;
    }

    private List<Maker> getListByQuery(String sql) {
        SQLiteOpenHelper helper = null;
        SQLiteDatabase db = null;
        try {
            helper = new DBHelper(context);
            db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            List<Maker> makers = new ArrayList<>();

            if (cursor.moveToFirst()) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    makers.add(getNextMaker(cursor));
                    cursor.moveToNext();
                }
            }
            return makers;
        } catch (SQLException e) {
            Toast.makeText(context, context.getString(R.string.trouble_db), Toast.LENGTH_SHORT);
        } finally {
            helper.close();
            db.close();
        }
        return null;
    }

    private Maker getNextMaker(Cursor cursor) {
        int id = cursor.getInt(0);
        String name = cursor.getString(1);
        String surname = cursor.getString(2);
        String patronymic = cursor.getString(3);
        String phoneNumber  = cursor.getString(4);
        String eMail = cursor.getString(5);
        String otherMeans = cursor.getString(6);

        return new Maker(id, name, surname, patronymic, phoneNumber, eMail, otherMeans);
    }
}
